# Title: Oracle 19c Deployment
# Requirements: Tested with Oracle Linux 8.4, Red Hat Linux 7.9/8.5 and Oracle 19c or 21c.
# Author: Michael J. Acosta
# Date: 10/27/2021
# Updated: 5/27/2022
# Notes: This is a snippet, not a script.
# Resource: https://www.oracle.com/database/technologies/oracle-database-software-downloads.html
: '
Requirements:
    Make sure to edit vim /etc/yum/pluginconf.d/search-disabled-repos.conf and set notify_only= to 0. This is needed especially on a minimal installation of rhel 

    For ERROR:
LISTENER:Port 1521 provided for this listener is currently in use.
Make sure that your instance of RHEL has a /etc/hosts. The example below can be used as reference:

127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.161  box-rhel1 box-rhel1.localdomain
'

# Login as root.
su

#################################
#           Firewalld           #
#################################

# Configure firewall.
# Managing Zones.
firewall-cmd --get-zones
firewall-cmd --list-all-zones #detailed info for each zone.
firewall-cmd --get-default-zone
firewall-cmd --set-default-zone=external
firewall-cmd --get-active-zones

# Remove interface from zone if needed.
firewall-cmd --permanent --zone=trusted --remove-interface=eth0
# Add interface to zone if needed.
firewall-cmd --zone=external --permanent --change-interface=ens160

# Remove cockpit if enabled and add 1521.
firewall-cmd --zone=trusted --permanent --remove-service cockpit
firewall-cmd --zone=trusted --permanent --add-port 1521/tcp
firewall-cmd --zone=trusted --permanent --add-port 22/tcp
firewall-cmd --zone=trusted --permanent --remove-port=111/tcp

# Rich Rules (Prefered) - Only use port 22 if you want to keep ssh and 1521 for oracledb.
firewall-cmd --add-rich-rule='rule family=ipv4 source address=0.0.0.0/0 port port=9090 protocol=tcp reject' --permanent
firewall-cmd --add-rich-rule='rule family=ipv4 source address=0.0.0.0/0 port port=5500 protocol=tcp reject' --permanent
firewall-cmd --add-rich-rule='rule family=ipv4 source address=0.0.0.0/0 port port=53 protocol=tcp reject' --permanent
firewall-cmd --add-rich-rule='rule family=ipv4 source address=0.0.0.0/0 port port=111 protocol=tcp reject' --permanent
firewall-cmd --add-rich-rule='rule family=ipv4 source address=0.0.0.0/0 port port=22 protocol=tcp accept' --permanent
firewall-cmd --add-rich-rule='rule family=ipv4 source address=0.0.0.0/0 port port=1521 protocol=tcp accept' --permanent
firewall-cmd --add-rich-rule='rule family=ipv4 source address=0.0.0.0/0 port port=5432 protocol=tcp accept' --permanent
firewall-cmd --add-rich-rule='rule family=ipv4 source address=0.0.0.0/0 port port=5432 protocol=tcp accept' --permanent
firewall-cmd --add-rich-rule='rule family=ipv4 source address=0.0.0.0/0 port port=1433 protocol=tcp accept' --permanent

# Web Ports
firewall-cmd --add-rich-rule='rule family=ipv4 source address=0.0.0.0/0 port port=80 protocol=tcp accept' --permanent
firewall-cmd --add-rich-rule='rule family=ipv4 source address=0.0.0.0/0 port port=443 protocol=tcp accept' --permanent

# Disable ICMP
firewall-cmd --add-rich-rule='rule protocol value=icmp reject' --permanent

firewall-cmd --reload
systemctl restart firewalld
firewall-cmd --state
firewall-cmd --zone=external --list-all
firewall-cmd --zone=external --list-rich-rules


#################################
#     Oracle DB Installation    #
#################################
# RHEL 8.4+ requires the following packages. (Use these dependencies only if you are off the Major release version)
yum install -y http://mirror.centos.org/centos/7/os/x86_64/Packages/compat-libcap1-1.10-7.el7.x86_64.rpm
yum install -y http://mirror.centos.org/centos/7/os/x86_64/Packages/compat-libstdc++-33-3.2.3-72.el7.x86_64.rpm
yum install -y http://mirror.centos.org/centos/7/os/x86_64/Packages/glibc-2.17-317.el7.x86_64.rpm
ln -s /lib64/libnsl.so.2 /lib64/libnsl.so.1
# RHEL 7.9+ requires the following packages.
yum install -y http://mirror.centos.org/centos/7/os/x86_64/Packages/compat-libstdc++-33-3.2.3-72.el7.x86_64.rpm

# Install Oracle 19c
yum install -y  https://yum.oracle.com/repo/OracleLinux/OL7/latest/x86_64/getPackage/oracle-database-preinstall-19c-1.0-1.el7.x86_64.rpm
yum localinstall -y ./oracle-database-ee-19c-1.0-1.x86_64.rpm
sing
# Optional
yum localinstall -y ./jdk-17_linux-x64_bin.rpm
yum localinstall -y ./sqldeveloper-21.4.2-018.1706.noarch.rpm

# Install Oracle 21c (Tested on RHEL 9)
yum install -y https://vault.centos.org/centos/8/AppStream/x86_64/os/Packages/compat-openssl10-1.0.2o-3.el8.x86_64.rpm
yum localinstall -y ./oracle-database-preinstall-21c-1.0-1.el8.x86_64.rpm
yum localinstall -y ./oracle-database-ee-21c-1.0-1.ol8.x86_64.rpm
# Optional
yum install -y java-11-openjdk.x86_64
yum localinstall -y ./sqldeveloper-21.4.3-063.0100.noarch.rpm


#################################
#      Exports & Aliases        #
#################################
#Oracle 19c
# Bash, Zsh Export(re-use for all your users.)
# Following alias is for Oracle XE.
echo '' >> $HOME/.bashrc; echo '#Oracle Environment Variables' >> $HOME/.bashrc
echo 'export PATH="/opt/oracle/product/19c/dbhome_1/bin:$PATH"' >> $HOME/.bashrc
echo 'export ORACLE_SID=ORCLCDB' >> $HOME/.bashrc
echo 'export ORAENV_ASK=NO' >> $HOME/.bashrc
echo 'export ORACLE_HOME="/opt/oracle/product/19c/dbhome_1"' >> $HOME/.bashrc

# The following alias is for Oracle SQL Developer.
echo '' >> $HOME/.bashrc; echo '#Oracle SQL Developer alias' >> $HOME/.bashrc
echo 'alias sqldeveloper="/usr/local/bin/sqldeveloper"' >> $HOME/.bashrc

source $HOME/.bashrc

#Oracle 21c
# Bash, Zsh Export(re-use for all your users.)
# Following alias is for Oracle XE.
echo '' >> $HOME/.bashrc; echo '#Oracle Environment Variables' >> $HOME/.bashrc
echo 'export PATH="/opt/oracle/product/21c/dbhome_1/bin:$PATH"' >> $HOME/.bashrc
echo 'export ORACLE_SID=ORCLCDB' >> $HOME/.bashrc
echo 'export ORAENV_ASK=NO' >> $HOME/.bashrc
echo 'export ORACLE_HOME="/opt/oracle/product/21c/dbhome_1"' >> $HOME/.bashrc

# The following alias is for Oracle SQL Developer.
echo '' >> $HOME/.bashrc; echo '#Oracle SQL Developer alias' >> $HOME/.bashrc
echo 'alias sqldeveloper="/usr/local/bin/sqldeveloper"' >> $HOME/.bashrc

source $HOME/.bashrc


#################################
#     Oracle DB Configuration   #
#################################
: '
NOTE!!
For ERROR:
LISTENER:Port 1521 provided for this listener is currently in use.
Make sure that your instance of RHEL has a /etc/hosts. The example below can be used as reference.
Change the last line to your localhost ip and dns name

127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.1.161  box-rhel1 box-rhel1.localdomain

For ERROR:
[WARNING] [INS-08109] Unexpected error occurred while validating inputs at state 'DBCreationOptions'

'
#Oracle 19c
# Configure Oracle DB(setup password for sys).
/etc/init.d/oracledb_ORCLCDB-19c configure
/etc/init.d/oracledb_ORCLCDB-19c start
/etc/init.d/oracledb_ORCLCDB-19c status

#Oracle 21c
# Configure Oracle DB(setup password for sys).
/etc/init.d/oracledb_ORCLCDB-21c configure
/etc/init.d/oracledb_ORCLCDB-21c start
/etc/init.d/oracledb_ORCLCDB-21c status

# Configure systemd
systemctl enable --now oracledb_ORCLCDB-19c.service
systemctl status oracledb_ORCLCDB-19c.service

#################################
#      Oracle DB Connecting     #
#################################
# Connect to the local oracledb via shell.
/opt/oracle/product/19c/dbhome_1/bin/sqlplus sys as sysdba
# Forgotten password accounts.
sudo su oracle
/opt/oracle/product/19c/dbhome_1/bin/sqlplus / as sysdba
# Reset Password
alter user sys identified by [new password];

# Check Net Services Listener.
lsnrctl status
netstat -plant | grep 1521
nmap localhost

#################################
#     Deinstalling OracleDB     #
#################################
/etc/init.d/oracledb_ORCLCDB-19c delete
yum remove -y oracle-database-preinstall-19c-1.0-1.el7.x86_64
yum remove -y oracle-database-ee-19c-1.0-1.x86_64
rm -rf /opt/oracle
reboot
